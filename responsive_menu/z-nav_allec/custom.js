	$('.z-nav__list').mobileMenu({
	    triggerMenu:'.z-nav__toggle',
		subMenuTrigger: ".z-nav__toggle-sub",
		animationSpeed:500	
	});

	// Mobile toggle triger
	$('.z-nav__toggle').on('mousedown touchstart', function (){
		$('.z-nav__toggle').toggleClass('open-nav');
		var $mobileNav = $('.z-nav__list');

		if($mobileNav.hasClass('open-nav')){
			$mobileNav.removeClass('open-nav close-nav');
			$mobileNav.addClass('close-nav');
		}
		else{
			$mobileNav.removeClass('open-nav close-nav');
			$mobileNav.addClass('open-nav');
		}
	});